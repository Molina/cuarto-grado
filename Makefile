SHELL := sh -e

SCRIPTS = "debian/preinst install" "debian/postinst configure" "debian/prerm remove" "debian/postrm remove"

all: build

test:

	@echo -n "\n===== Comprobando posibles errores de sintaxis en los scripts de mantenedor =====\n\n"

	@for SCRIPT in $(SCRIPTS); \
	do \
		echo -n "$${SCRIPT}\n"; \
		bash -n $${SCRIPT}; \
	done

	@echo -n "\n=================================================================================\nHECHO!\n\n"

build:

	@echo "Nada para compilar!"

install:

	mkdir -p  $(DESTDIR)/usr/share/contenido-educativo/4toGrado/png/
	cp -r  png/* $(DESTDIR)/usr/share/contenido-educativo/4toGrado/png/

	mkdir -p  $(DESTDIR)/usr/share/applications/	
	cp -r desktop/* $(DESTDIR)/usr/share/applications/

uninstall:


clean:

reinstall: uninstall install
